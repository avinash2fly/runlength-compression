/*
 * @author : avinash.gupta | Z5121685
 */
#include <stdio.h>

/*function used to write ascii value to file
 * @param  :
 * ascii -> can be count or character
 * file -> file object to which content need to be written
 * isChar-> just to distinguish between character and count
 * 1-> means it is a character
 * 0-> means it is a count
 *
 * */

unsigned int write_acii_to_file( int ascii,FILE* file,short isChar){
	if (!isChar)
	{
		//add 128 to enable the msb bit, so that it can be distinguished as a count while decoding.
		ascii = ascii + 128;
	}
	//writing just one byte into the file
	fwrite(&ascii,1,1,file);
	return 0;
}

/*
 * function used to open file and return reference to file object
 * @param:
 * filename : name of the file
 */
FILE* open_File(char filename[])
{
	FILE *fr;
	//open file in read mode
	fr = fopen(filename, "r");
	if (fr == NULL)
	{
		//printf("The file didn't open.\n");
		return 0;
	}
	//return file object
	return fr;
}

/*
 * general function function used to print characterand it's count on file
 * @param :
 * ch : character to print
 * count : count of the character (ch)
 * isFile : boolean to define whether to write in a file or console
 * file : file object reference, where data need to be written.
 */
unsigned int print_out(char ch,unsigned int count,short isFile,FILE* file){
	if(isFile){
		//write data into file
		if(count>2){
			//write character in file
			write_acii_to_file(ch,file,1);
			//reduce count by three as 0->3 and so on to achieve maximum compression
			count = count-3;
			//divide number into chunks of 127 and write into file
			while(count>127){
				write_acii_to_file(127,file,0);
				count=count-127;
			}
			//write the rest count/count less than equal to 127
			write_acii_to_file(count,file,0);


		}
		else{
			// if repeatation is less than three times just repeat the character
			unsigned int i;
			for(i=0;i<count;i=i+1){
				write_acii_to_file(ch,file,1);
			}
		}
	}
	else{
		//used to print in stdout / console
		if(count>2){
			printf("%c",ch);
			printf("[%d]",(count-3));
		}
		else{
			unsigned int i;
			for(i=0;i<count;i=i+1){
				printf("%c",ch);
			}
		}
	}

	return 0;
}

/*
 * function used for encoding the file
 * @param :
 * input[] : name of input file
 * isFile : whether to write in a file(1) / console (0)
 * output[] : output file where data need to be written when isFile is set to 1 or it won't check
 */
unsigned int rlencode(char input[],short isFile,char output[]){
	//reference to file object where data need to be written
	FILE *fileOut;
	if (isFile){
		//intialize only when data need to be written in file
		fileOut = fopen (output, "w+");
	}
	//open input file in read mode
	FILE *file = open_File(input);
	//character to count
	char c;
	//tmp holder for previous character
	char tmp;
	unsigned int count=0;
	//keep on parsing character and count it
	while(1){
		c= fgetc(file);
		//break when reach to end of file
		if(feof(file)){
			print_out(tmp,count,isFile,fileOut);
			break;
		}
		if(tmp!=c && tmp!=0){
			print_out(tmp,count,isFile,fileOut);
			count=0;
		}
		count=count+1;
		tmp=c;
	}
	//close file
	fclose(file);
	if(isFile)
		fclose(fileOut);
	return 0;
}
/*
 * main program
 */
int main(int argc, char **argv) {

	if(argc==3)
		//to print in file
		rlencode(argv[1],1,argv[2]);
	else
		//to print in console
		rlencode(argv[1],0,"");

}
