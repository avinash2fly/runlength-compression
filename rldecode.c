/*
 * @author : avinash.gupta | Z5121685
 */
#include <stdio.h>

/*
 * function used to open file and return reference to file object
 * @param:
 * filename : name of the file
 */
FILE* open_File(char filename[])
{
	FILE *fr;
	//open file in read mode
	fr = fopen(filename, "r");
	if (fr == NULL)
	{
		//printf("The file didn't open.\n");
		return 0;
	}
	//return file object
	return fr;
}
/*
 * function is used to print the repetition of character in file
 * @param:
 * file : file reference where data need to be written
 * count : count of character
 * c : character to write in file
 */
unsigned int print_multiple(FILE *file,unsigned int count,char c){
	unsigned int i;
	//print repetion of character
	for(i=0;i<(count+2);i++)
		fprintf(file, "%c", c);

	return 0;
}

/*
 * function used to decode the file and create the original file back
 * @param:
 * input[] : the encode file to decrypt
 * isFile : whether to ouput in file (1) / console (0)
 * outputp[] : name of the output file where data need to be written only if isFile is set to 1.
 */
unsigned int rldecode(char input[],short isFile,char output[]){
	FILE *fileOut;
	unsigned int last_char;
	char buffer[1];
	int decimal_val=0;
	//open file in write mode where data need to be written
	if (isFile){
		fileOut = fopen (output, "w+");
	}
	//open input file in read mode
	FILE *file = open_File(input);
	//hold number of character
	unsigned int count=0;
	//temporary bit set to differentiate whether count is set or not. so that count can be added instead if reset.
	short count_set=0;
	//int temp;
	//read file byte by byte
	while (fread(buffer, 1, 1, file) == 1) {
		decimal_val = (int)buffer[0];
		//decimal_val = temp;
		if (decimal_val<0){
			//setting count where decimal value is negative as msb is set which mean it's a signed number
			count=count + (decimal_val+128);
			count_set=1;
		}
		else{
			if(count_set){
				//if count is set then print the character for count-1 times as once was already printed
				if(isFile){
					//print in file
					print_multiple(fileOut,count,last_char);
				}
				else
					//print in console
					printf("[%d]",count);
			}
			//print character once and then keep track of count
			if(isFile)
				//print in file
				fprintf(fileOut,"%c",decimal_val);
			else
				//print in console
				printf("%c",decimal_val);
			//count reset
			count=0;
			count_set=0;
			//track last character
			last_char = decimal_val;
		}

	}
	//printinf the last count of last digit as loop will exit before printing last count for last character
	if(count_set){
		if(isFile){
			//print in file
			print_multiple(fileOut,count,last_char);
		}
		else
			//print in console
			printf("[%d]",count);
	}
	//close files
	fclose(file);
	if(isFile)
		fclose(fileOut);
	return 0;
}

/*
 * main program
 */
int main(int argc, char **argv) {
	if(argc==3)
		// for writing output in file
		rldecode(argv[1],1,argv[2]);
	else
		//for writing in console
		rldecode(argv[1],0,"");

}
