All : rlencode rldecode

rlencode : rlencode.o
	gcc rlencode.o -o rlencode

rlencode.o : rlencode.c
	gcc -c rlencode.c 

rldecode : rldecode.o
	gcc rldecode.o -o rldecode

rldecode.o :  rldecode.c
	gcc -c rldecode.c 

clean : 
	rm *.o rlencode

clean : 
	rm *.o rldecode
